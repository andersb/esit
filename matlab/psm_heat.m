%
% Heat map for polarization-space modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg                        The configuration structure:
%
% Output:
%
%    map                        The heat map
%
function heat_map = psm_heat(cfg)

%
% Heat map simulation
%

% Ranges in x- and y-direction
x_rng = cfg.x_rng;
y_rng = cfg.y_rng;

% Allocate heat map
xr = length(x_rng);
yr = length(y_rng);
heat_map = zeros(xr,yr);

% Find the required slot length
cfg.p_rx(1) = max(x_rng);
cfg = ris_amp_dly(cfg);
[cfg.slot_len,~,~] = psm_slot_len(cfg);

% Generate random data and carrier
[cfg,carrier] = psm_source(cfg);

fprintf('\n#######################################################\n')
fprintf('Heat map using %d slots\n',...
           cfg.num_code_words)
fprintf('#######################################################\n')

% Loop for the positions
tic 
for x = x_rng
    iy = 1;
    for y = y_rng

        fprintf('Position: x = %.1f and y = %.1f meters\n',x,-y)

        % Compute next position
        cfg.p_rx(1) = x;
        cfg.p_rx(2) = -y;

        % Attenuation and time/phase delay: A(m,n), Tau(m,n), and Phase(m,n)
        cfg = ris_amp_dly(cfg);   

        % Compute channel response
        cfg.TotPhase = cfg.Phase_m_n + cfg.phi;
        shift = cfg.A_m_n .* exp(-1j*cfg.TotPhase);
        cfg.mag_phase_shift = shift;

        % Slot length
        [cfg.slot_len,samp_spread,ris_dly] = psm_slot_len(cfg);

        % Compute range of samples to use
        cfg.sample_range = (1:cfg.K) - 1 + samp_spread;
        
        % Convert delay to sample offset
        cfg.ris_samp_dly = round(cfg.Fs * ris_dly);

        % Loop for number of time slots
        pwr = 0;
        kx = 1:cfg.slot_len;
        for cx = 1:cfg.num_code_words
    
            % Modulate the carrier wave
            slot = tx_psm(cfg,carrier(kx),cfg.epsilon(cx),cfg.delta_phase(cx)); 
            kx = kx + cfg.slot_len;
    
            % Generate the snapshots
            sig = rx_snap(cfg,slot);
            pwr = pwr + sum(sig .* conj(sig),'all');

        end % time slots

        % Save power
        heat_map(x,iy) = pwr / cfg.num_code_words / numel(sig);

        % Update index
        iy = iy+1;
                   
    end % for y-direction
end % for x-direction

dur = toc / 60;

disp(sprintf('\n#####################################################'))
disp(sprintf('The heat map simulation took %.3f minutes',dur))
disp(sprintf('#####################################################'))

% EOF
