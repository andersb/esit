%
% Receive K snapshots and compute the covariance matrix
%
% Anders Buvarp
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg                Configuration
%    sig                Signal
%
% Output:
%
%    out                Horizontal and vertical samples
%

function out = rx_snap(cfg,sig)

% Allocate output
out = zeros(2,cfg.K);

% Obtain magnitudes and phase shifts for the RIS paths
mp = cfg.mag_phase_shift;

% Get the index to start
start = cfg.sample_range(1) - cfg.ris_samp_dly;
        
% Loop for K realizations
for kx = 1:cfg.K

    % Extract RIS samples at time = delay offset + kx
    sx = start+kx-1;

    % Extract RIS samples
    s = sig(1,:);
    bb_h = mp .* s(sx);
    s = sig(2,:);
    bb_v = mp .* s(sx);
    
    % Add all reflected contributions coherently
    bb_h = sum(sum(bb_h));
    bb_v = sum(sum(bb_v));
        
    % Store
    out(1,kx) = bb_h;
    out(2,kx) = bb_v;
    
end % for

% Check if we plot
if cfg.plot_flag
    
    % Compute RIS and LOS signals
    los_s = los_mp * sig(1,cfg.los_sample_range);
    los_s = sig(1,cfg.los_sample_range);
    ris_s0 = sig(2,cfg.los_sample_range);
    ris_s1 = sig(3,cfg.los_sample_range);
    
    figure(67)
    plot(abs(los_s),'kx-'),hold,grid
    plot(abs(ris_s0),'bo-')
    plot(abs(ris_s1),'r*-'),hold
    xlabel('Samples')
    ylabel('Magnitude')
    title('The magnitude of the 48 input samples used by the MUSIC algorithm')
    l0 = sprintf('LOS at %.1f degrees',los_ang*180/pi);
    l1 = sprintf('RIS Edge 0 at %.1f Degrees',cfg.Gamma0*180/pi);
    l2 = sprintf('RIS Edge 1 at %.1f Degrees',cfg.Gamma1*180/pi);
    legend(l0,l1,l2)
    
    figure(68)
    plot(angle(los_s)/pi,'kx-'),hold,grid
    plot(angle(ris_s0)/pi,'bo-')
    plot(angle(ris_s1)/pi,'r*-'),hold
    xlabel('Samples')
    ylabel('Phase / PI')
    title('The phase of the 48 input samples used by the MUSIC algorithm')
    l0 = sprintf('LOS at %.1f degrees',los_ang*180/pi);
    l1 = sprintf('RIS Edge 0 at %.1f Degrees',cfg.Gamma0*180/pi);
    l2 = sprintf('RIS Edge 1 at %.1f Degrees',cfg.Gamma1*180/pi);
    legend(l0,l1,l2)
    
end % if

% EOF
