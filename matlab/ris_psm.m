%
% Reconfigurable Intelligent Surfaces for polarization-space modulation
%
% Anders Buvarp
%
% References:
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%       cfg                 Configuration
%       tx_pos_x            Position of transmitter in X-direction
%       tx_pos_y            Position of transmitter in Y-direction
%       rx_pos_x            Position of receiver in X-direction
%       rx_pos_y            Position of receiver in Y-direction
%
% Output:
%
%       ber                 Bit error rate
%       map                 Heat map
%

function res = ris_psm(cfg,tx_pos_x,tx_pos_y,rx_pos_x,rx_pos_y)

% Init RIS geometry, gains, etc.
cfg = ris_psm_init(cfg,tx_pos_x,tx_pos_y,rx_pos_x,rx_pos_y);

% Check if generating heat map
if cfg.heat_flag
    % Make heat map
    res = psm_heat(cfg);
else
    % Monte Carlo simulations
    res = psm_mc(cfg);
end % if-else

% EOF
