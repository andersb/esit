%
% Reconfigurable Intelligent Surfaces for Polarization-space modulation
%
% MATLAB script to configure the RIS Monte Carlo model
%
% Anders Buvarp
%
% References:
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

clear

% Flags
cfg.verbose = true;
cfg.verbose = false;
cfg.heat_flag = 0;
cfg.plot_flag = 0;
cfg.save_src_flag = 0;

% Monte-Carlo parameters
param_type = 'PSM';
num_iter = 128;
cfg.num_code_words = 2048;
symb_rng = 1:cfg.num_code_words;
<<<<<<< HEAD
<<<<<<< HEAD
snr_rng = -5:16;
=======
snr_rng = [0 3 6 10];
>>>>>>> parent of 460d92b... Adding data 11 x 2048 x 64
=======
snr_rng = [0 3 6 10];
>>>>>>> parent of 460d92b... Adding data 11 x 2048 x 64
    
% Code word length [bits]
cfg.code_word_len = 4;

% Number of snapshots
cfg.K = 480;

% Source magnitude
cfg.E0 = 1;                                     

% 5G-NR FR2, Band n260, K_a-band
cfg.Fc = 39e9;                              % Carrier frequency
cfg.c0 = 3e8;                               % Speed of light
cfg.wavelength = cfg.c0 / cfg.Fc;           % Wavelength
cfg.Omega_c = 2*pi*cfg.Fc;                  % Angular velocity

% Element spacing
cfg.d_y = cfg.wavelength/5;
cfg.d_z = cfg.wavelength/5; 

% XY-coordinates  
tx_pos_x = 20;
tx_pos_y = 0;
rx_pos_x = 20;
rx_pos_y = -260;                % Fraunhofer region is 2D^2 / wavelength

% Number of rows/columns
s = 1;                          % RIS has dimensions 1 meter x 1 meter
cfg.E.rows = round(s/cfg.d_z);
cfg.E.cols = round(s/cfg.d_y);

% Sample rate
cfg.over_sampling = 8;
cfg.Fs = cfg.Fc*cfg.over_sampling;
cfg.Ts = 1/cfg.Fs;

% Down-conversion vector
ix = 1:cfg.K;
ix = ix-1;
T = cfg.Ts*ix;
Fdc = 37.78125;
Fdc = 36.5625e9;
dc = exp(-2j*pi*Fdc*T);
cfg.down_conversion = dc;

% Decimation rate
cfg.decimation_rate = cfg.over_sampling;
cfg.decimation_filt_len = 50;

% Antenna gains
G.tx_m_n_dBi = 0;                      % Antenna gain [dBi]; Tx in the RIS element direction
G.m_n_tx_dBi = 0;                      % Antenna gain [dBi]; RIS in direction of Tx
G.rx_m_n_dBi = 0;                      % Antenna gain [dBi]; Rx in the RIS element direction
G.m_n_rx_dBi = 0;                      % Antenna gain [dBi]; RIS in direction of Rx
G.mu = 1;                              % The fraction of the incident energy that is scattered
cfg.G = G;

% Set-up the configuration
cfg.num_iter = num_iter;
cfg.param_type = param_type;
cfg.symb_range = symb_rng;
cfg.snr_range = snr_rng;
cfg.x_rng = 1:rx_pos_x+5;
cfg.y_rng = 1:-rx_pos_y;

% Generate heat map or run the Monte Carlo model
res = ris_psm(cfg,tx_pos_x,tx_pos_y,rx_pos_x,rx_pos_y);

% Check if heat map
if cfg.heat_flag

    % Save results
    dBm = 10*log10(res'*1000);
    save dBm.mat dBm

else

    % Compute bit error rate
    num_bits = num_iter*cfg.num_code_words*cfg.code_word_len;
    ber = sum(res,[2 3]) / num_bits;

    % Save results
    save ../data/ber.mat ber

end % if-else

% EOF
