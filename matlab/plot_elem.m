%
% Reconfigurable Intelligent Surfaces
% Plot the RMS error vs element utilization
%
%
% Anders Buvarp
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    id             The ID number of the plot
%    rms_equi       The data to be plotted
%    rms0           The data to be plotted
%    cfg            The configuration
%
function plot_elem(id,rms_equi,rms0,cfg)

% Range of parameters
prng = cfg.param_range;

figure(id)


plot(prng,rms_equi,'r-.'),grid,hold
plot(prng,rms0,'b'),hold

xlabel('Number of element columns used at each end of the RIS')
ylabel('Root Mean Square Error')

legend('(a) RIS azimuth -30 degrees and LOS azimuth +30 degrees', ...
       '(b) RIS azimuth -31 degrees and LOS azimuth 0 degrees')

% Make sure the Y-axis starts at the origin
axis([ 0 2500 0 61 ]);

% Re-size
set(gcf,'units','points','position',[100,100,500,300])

% EOF
