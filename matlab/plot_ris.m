%
% Reconfigurable Intelligent Surfaces Plots
%
%
% Anders Buvarp
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    id             The ID number of the plot
%    rms            The data to be plotted
%    cfg            The configuration
%
function plot_ris(id,rms,cfg)

% Range of parameters
prng = cfg.param_range;

% ULA size
N = cfg.N;

figure(id)

s = size(rms,1);
if s < 5
    rms((s+1):5,:) = zeros(5-s,size(rms,2));
end % if

if s == 1
    plot(prng,rms(1,:),'r-.'),grid
else
    plot(prng,rms(1,:),'r-d'),grid,hold
    plot(prng,rms(2,:),'g-*')
    plot(prng,rms(3,:),'b-o')
    plot(prng,rms(4,:),'m-x')
    plot(prng,rms(5,:),'k-s'),hold
end % if-else

% Check if we use correlated signal
model = ', Uncorrelated Signals';
if cfg.corr_flag
    model = ', Correlated Signals';
end % if

if cfg.ris_model_flag
    model = ', RIS Model';
end % if 

% Check parameter type
switch cfg.param_type
    case 'Elements'
        % RIS phase
        %title(sprintf('ULA N=%d   DF Error vs Number of Element Columns Used%s',N,model))
        xlabel('Number of element columns used at each end of the RIS')
    case 'Phase'
        % RIS phase
        title(sprintf('ULA N=%d   DF Error vs Phase of RIS Wave%s',N,model))
        xlabel('Phase of RIS Wave [degrees]')
    case 'AoA'
        % RIS azimuth
        title(sprintf('ULA N=%d   DF Error vs Angle of RIS Wave%s',N,model))
        xlabel('Azimuth of RIS Wave [degrees]')
    case 'Delay'
        % RIS delay
        title(sprintf('ULA N=%d   DF Error vs Delay of RIS Wave%s',N,model))
        xlabel('Delay of RIS Wave [microseconds]')
    otherwise
        disp('Invalid parameter type')
        return
end % switch

ylabel('Root Mean Square Error')

if s == 1
    legend(sprintf('SNR = %d dB',cfg.snr_range))
    legend('(a) RIS azimuth -30 degrees and LOS azimuth +30 degrees', ...
           '(b) RIS azimuth -31 degrees and LOS azimuth 0 degrees')
else
    legend('SNR = -10 dB','SNR = -5 dB','SNR = 0 dB', 'SNR = +5 dB', 'SNR = +10 dB')
end % if-else

% Make sure the Y-axis starts at the origin
axis([ 0 2500 0 61 ]);

% Re-size
set(gcf,'units','points','position',[100,100,500,300])

% EOF
