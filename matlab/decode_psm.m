%
% Generate time slot to be transmitted
%
% Anders Buvarp
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg        Configuration
%    sig        Received signal
%    slot       Slot number
%
% Output:
%
%    data       Decoded data
%    err        Number of bit errors
%

function [data,err] = decode_psm(cfg,sig,slot)

% Decode slant angle
conj_s1 = conj(sig(1,:));
a = sig(2,:) .* conj_s1;
%s = sum(atan2(imag(a),real(a)));
%if 2*abs(s) > pi*cfg.K
s = median(atan2(imag(a),real(a)));
ep = 1;
if abs(s) > pi/2
    ep = -1;
end % if

% Decode phase difference
p = ep*a;
%pd = sum(atan2(imag(p),real(p)));
%rx = round(ep*16*pd/pi/cfg.K);
pd = median(atan2(imag(p),real(p)));

% Convert to bits
rx = round(ep*16*pd/pi);
if rx > 7
    rx = 7;
end % if
if rx < 0
    rx = 0;
end % if

% Bit errors
err = (ep ~= cfg.epsilon(slot));        % Slant angle bit
tx_bits = de2bi(cfg.lsb3(slot),3);      % Phase bits
rx_bits = de2bi(rx,3);
diff_bits = tx_bits - rx_bits;
err = err + sum(abs(diff_bits));

% Output decoded data
data = 8*(1-ep)/2 + rx;

% EOF