%
% Compute the distance to the RIS elements from the position, p
%
% Anders Buvarp
%
% References:
%
% [1] https://arxiv.org/abs/2006.06991
%
% Virginia Tech National Security Institute
%
%
% Input:
%
%    cfg           Configuration
%    p             The position to compute the distance to
%
% Output:
%
%    d             Distance
%

function d = ris_norm(cfg,p)

x = p(1);
y = p(2) - cfg.y_pos_ris;
z = p(3) - cfg.z_pos_ris;              % RIS is placed on Y-axis (x=0)

% Repeat y for P rows
P = cfg.E.rows;
y = repmat(y',1,P)';

% Repeat z columns
num_c = size(y,2);
z = repmat(z',1,num_c);

% Square, sum and sqrt-root
d = sqrt(x.^2 + y.^2 + z.^2);

% EOF