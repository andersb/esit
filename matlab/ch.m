%
% AWGN Channel
%
% Anders Buvarp
%
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    cfg     Configuration
%    iq      I/Q samples
%    snr     Signal-to-Noise Ratio
%
% Output:
%
%    out     Channel output
%

function out = ch(cfg,iq,snr)

% Get signal power
P = 2;
N = size(iq,2);
sig_pwr = trace(iq*iq') / (N*P);

% Linear SNR
lin_snr = 10^(snr/10);

% Get noise power
cfg.noise_pwr = sig_pwr / lin_snr;

% Standard deviation
% 50% power each in I and Q
s = sqrt(cfg.noise_pwr/2);

% Generate noise
awgn = randn(P,N)+1j*randn(P,N);
awgn = s*awgn;

% Channel output
out = iq + awgn;

% EOF