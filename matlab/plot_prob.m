%
% Plot of probability of detection for 1, 2 and 3 strongest peaks
%
%
% Anders Buvarp
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    id             The ID number of the plot
%    Pd             The data to be plotted
%    cfg            The configuration
%
function plot_prob(id,Pd,cfg)

% Range of SNR
srng = cfg.snr_range;

% ULA size
N = cfg.N;

figure(id)

mpeaks = 3;
npeaks = size(Pd,2);
nsnr = size(Pd,1);
if npeaks < 3
    Pd(:,(npeaks+1):mpeaks) = zeros(nsnr,mpeaks-npeaks);
end % if

% Correlated
plot(srng,Pd(:,1),'-.b'),grid,hold
plot(srng,Pd(:,2),'.-g')
plot(srng,Pd(:,3),'--k')
plot(srng,Pd(:,4),'-r')

% Uncorrelated
plot(srng,Pd(:,5),'-.b')
plot(srng,Pd(:,6),'.-g')
plot(srng,Pd(:,7),'--k')
plot(srng,Pd(:,8),'-r'),hold

% Check if we are using the RIS model
model = '';
if cfg.ris_model_flag
    model = ', RIS Model';
end % if 

title(sprintf('Probability of Detection vs SNR',N,model))
xlabel('SNR [dB]')
ylabel('Probability of Detection')
legend('Strongest Peak',...
       'Two Strongest Peaks',...
       'Three Strongest Peaks',...
       'Any Peak')

% Make sure the Y-axis starts at the origin
axis([ -25 25 0 1.02 ]);

% Re-size
set(gcf,'units','points','position',[100,100,500,300])

% EOF
