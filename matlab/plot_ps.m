%
% Reconfigurable Intelligent Surfaces 
% Plot the pseudo-spectrum
%
% Anders Buvarp
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    id             The ID number of the plot
%    ps_equi        The psuedo-spectrum for a equilateral triangle
%    ps0            The psuedo-spectrum for the 0-degree azimuth triangle
%    cfg            The configuration
%
function plot_ps(id,ps_equi,ps0)

% Setup test angles of arrival
W = 180;
aoa = (-W:W)/2;

figure(id)

plot(aoa,ps_equi,'r-.'),grid,hold
plot(aoa,ps0,'b'),hold

xlabel('Azimuth Angle [degrees]')
ylabel('Pseudo-spectrum')

legend('(a) RIS azimuth -30 degrees and LOS azimuth +30 degrees',...
       '(b) RIS azimuth -31 degrees and LOS azimuth 0 degrees')

% Make sure the Y-axis starts at the origin
axis([ -90 90 0 1.1 ]);

% Re-size
set(gcf,'units','points','position',[100,100,500,300])

% EOF
