%
% RIS specification that depends on some design parameter
%
% Anders Buvarp
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    tbd        Number of tbd
%
% Output:
%
%    cfg        Configuration
%

function cfg = ris_psm_spec(cfg,tbd)

x = cfg.p_rx(1);
y = -cfg.p_rx(2);
eta = atan2(x,y);
cfg.Gamma0 = -eta;

% EOF