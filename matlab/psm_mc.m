%
% Monte Carlo Model for polarization-space modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg                        The configuration structure:
%    num_iter                   Number of Monte-Carlo iterations
%    param_type                 'PSM'
%    param_range                Parameter range to sweep
%    snr_range                  SNR range
%    ref_angle                  Angle to compare the estimated AoA with
%
% Output:
%
%    rms                        Root-Mean Sequare Error
%    Pd                         Probability of detection
%    dist                       Peak magnitude distribution
%
function ber = ris_mc(cfg)

% Monte-Carlo Configuration
num_iter = cfg.num_iter;
param_type = cfg.param_type;
symb_range = cfg.symb_range;
snr_range = cfg.snr_range;

if cfg.verbose

    %
    % Print Monte-Carlo simulation parameters
    %

    disp(sprintf('\nTransmitter located at:\t\t\t\t(%.3f,%.3f)',cfg.p_tx(1),cfg.p_tx(2)))
    disp(sprintf('Receiver located at:\t\t\t\t(%.3f,%.3f)',cfg.p_rx(1),cfg.p_rx(2)))
    disp(sprintf('RIS azimuth:\t\t\t\t%.3f',cfg.ris_azimuth))
    disp(sprintf('\nRIS dimensions:\t\t\t\t%d x %d elements',cfg.E.rows,cfg.E.cols))
    disp(sprintf('RIS dimensions:\t\t\t\t%.3f x %.3f meters',cfg.ris_height,cfg.ris_width))
    disp(sprintf('RIS spacing, d_y:\t\t\t%.3f [mm]',cfg.d_y*1000))
    disp(sprintf('RIS spacing, d_z:\t\t\t%.3f [mm]',cfg.d_z*1000))
    disp(sprintf('SNR Min:\t\t\t\t%d',snr_range(1)))
    disp(sprintf('SNR Max:\t\t\t\t%d',snr_range(end)))

    disp(sprintf('\nParam Type:\t\t\t\t%s',param_type))
    disp(sprintf('Param Min:\t\t\t\t%.3f',param_range(1)))
    disp(sprintf('Param Max:\t\t\t\t%.3f',param_range(end)))
    disp(sprintf('\nSample Rate:\t\t\t\t%.3f [Msamples/s]',cfg.Fs/1e6))
    disp(sprintf('Sample Period:\t\t\t\t%.3f [nanoseconds]',cfg.Ts*1e9))
    disp(sprintf('Wavelength:\t\t\t\t%.3f [mm]',cfg.wavelength*1000))
    
    disp(sprintf('\n#######################################################'))
    disp(sprintf('Monte Carlo with %d iterations for %d slots',num_iter,cfg.num_code_words))
    disp(sprintf('#######################################################'))
  
end % verbose

% Bit error rate vs SNR
s = length(snr_range);
a = length(symb_range);
ber = zeros(s,num_iter,a);

% Save quaternions to MATLAB file
% number of SNR levels x number of iterations x number of code words x number of quaternions
quat = zeros(s,num_iter,cfg.num_code_words,4*(cfg.K/cfg.decimation_rate));

% Generate random data and carrier
[cfg,carrier,src] = psm_source(cfg);

% Loop for the range of parameters
kx = 1:cfg.slot_len;
tic
symbs = [];
for symb = symb_range

    % Print slot
    fprintf('Slot: %d\tData: %d\n',symb,src(symb))
  
    % Modulate the carrier wave
    slot = tx_psm(cfg,carrier(kx),cfg.epsilon(symb),cfg.delta_phase(symb)); 
    kx = kx + cfg.slot_len;

    % Generate the snapshots
    ss = rx_snap(cfg,slot);

    % Down-conversion to baseband
    dc = ss .* cfg.down_conversion;
    h = decimate(dc(1,:),cfg.decimation_rate,cfg.decimation_filt_len,"fir"); 
    v = decimate(dc(2,:),cfg.decimation_rate,cfg.decimation_filt_len,"fir");
    bb = [h;v];
                 
    % Monte-Carlo iterations
    for count = 1:num_iter

        % Loop for range of SNR
        sx = 1;
        for snr = snr_range
                    
            % AWGN
            rx = ch(cfg,bb,snr);
            %rx = bb;

            % Save the received quaternions
            q = [real(rx(1,:)); imag(rx(1,:)); real(rx(2,:)); imag(rx(2,:)) ];
            q = reshape(q,1,numel(q));
            nrm = 1/4e-5;
            quat(sx,count,symb,:) = q*nrm;

            % Dump the symbol
            %symbs = [ symbs rx ];

            % Decode
            [data,err] = decode_psm(cfg,rx,symb);

            % Print decoded data and source
            if cfg.verbose
                fprintf('Source: %d\t\tDecoded Data: %d\t\tNum Bit Error(s): %d\n',src(px),data,err)
            end % if

            % Save bit errors
            ber(sx,count,symb) = err;

            % Update index to parameter
            sx = sx+1;

        end % for SNR

    end % for iter

end % for each symbol

% Save quaternions to file
num_snr = numel(snr_range);
savename = sprintf('quat_slots_%d_iter_%d_snrs_%d.mat',cfg.num_code_words,num_iter,num_snr);
%savename = sprintf('eval_quat_slots_%d_iter_%d.mat',cfg.num_code_words,num_iter);
fname = fullfile('../data', savename);
save(fname,'quat','src','snr_range','-v7.3');

% PSD
if cfg.plot_flag
    figure(10),pwelch(symbs(1,:),[],[],[],cfg.Fs/cfg.decimation_rate,'centered');
    figure(20),pwelch(symbs(2,:),[],[],[],cfg.Fs/cfg.decimation_rate,'centered');
end % if

dur = toc / 60;

disp(sprintf('\n#####################################################'))
disp(sprintf('The Monte-Carlo simulation took %.3f minutes',dur))
disp(sprintf('#####################################################'))

% EOF
