%
% Histogram with a logarithmic x-axis
%
% NOTE: This will not work in GNU Octave
%
% Anders Buvarp
%
% References:
%
% [1] https://www.mathworks.com/matlabcentral/answers/102406-how-can-i-plot-a-histogram-with-a-logarithmic-x-axis
%
% Virginia Tech National Security Institute
%

function loghist(id,x,color)

figure(id)
[~,edges] = histcounts(log10(x));
histogram(x,10.^edges,...%'Normalization','pdf',...
          'FaceColor',color,'EdgeColor',color)
set(gca, 'xscale','log')

% Alternative
%histogram(log10(x),nbins,color)
%xticklabels(num2cell(10.^get(gca,'XTick')));