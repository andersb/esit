%
% Plot bit error rate (BER) for polarization-space modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

% Load data
load ../data/ber.mat

% OFDM-BPSK BER
bpsk = [ .15 .1 .075 .055 .037 0.022 .012 .0055 .0025 .00075 .0002 .000035 ];

% SNR range
snr_rng = -5:10;

% Receiver position
rx_pos_x = 20;
rx_pos_y = -260;   % Fraunhofer region is 2D^2 / wavelength
x_rng = 1:rx_pos_x+5;
y_rng = 1:-rx_pos_y;

% Font and marker sizes
font_size = 40;
marker_size = 1;
line_width = 4;

% Plot BER
figure('color','w');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(snr_rng,ber,'x-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(-2:9,bpsk,'+-','LineWidth',line_width,'MarkerSize',marker_size),hold
axis square
xlabel('SNR [dB]')
ylabel('Bit Error Rate')
legend('RIS PSM','OFDM-BPSK')

% EOF
