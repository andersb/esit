%
% Plot DF error as a function of Delay for correlated and 
% uncorrelated signals
%
%
% Anders Buvarp
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    id             The ID number of the plot
%    rms_corr       DF error using correlated signals
%    rms_uncorr     DF error using uncorrelated signals
%    cfg            The configuration
%
function plot_dly(id,rms_corr,rms_uncorr,cfg)

figure(id)

num_snr = 5;
nsnr = size(rms_corr,1);
nparams = size(rms_corr,2);
if nsnr < num_snr
    rms_corr((nsnr+1):num_snr,:) = zeros(num_snr-nsnr,nparams);
    rms_uncorr((nsnr+1):num_snr,:) = zeros(num_snr-nsnr,nparams);
end % if

% Range of parameters in nanoseconds
prng = cfg.param_range*1000;

% Range of parameters in microseconds
%prng = cfg.param_range;

% ULA size
N = cfg.N;

%plot(prng,rms(1,:),'r-d'),grid,hold
%plot(prng,rms(2,:),'g-*')
%plot(prng,rms(3,:),'b-o')

subplot(211)
plot(prng,rms_corr(1,:),'r.-'),grid,hold
plot(prng,rms_corr(2,:),'b--')
plot(prng,rms_corr(3,:),'m-.')
plot(prng,rms_corr(4,:),'k:')
plot(prng,rms_corr(5,:),'g-'),hold
xlabel({'Delay [nanoseconds]','(a)'})
%xlabel({'Delay [microseconds]','(a)'})
ylabel('Root Mean Square Error')
%title(sprintf('ULA N=%d   Direction-Finding Error vs Delay using Correlated Signals',N))
legend('SNR = -15 dB', 'SNR = -10 dB', 'SNR = -5 dB', 'SNR = 0 dB', 'SNR = +5 dB')

% Make sure the Y-axis starts at the origin
y_max = ceil(max(max([rms_corr rms_uncorr])))
v = axis;
v = [ prng(1) prng(end) 0 y_max ];
axis(v)

subplot(212)
plot(prng,rms_uncorr(1,:),'r.-'),grid,hold
plot(prng,rms_uncorr(2,:),'b--')
plot(prng,rms_uncorr(3,:),'m-.')
plot(prng,rms_uncorr(4,:),'k:')
plot(prng,rms_uncorr(5,:),'g-'),hold
xlabel({'Delay [nanoseconds]','(b)'})
%xlabel({'Delay [microseconds]','(b)'})
ylabel('Root Mean Square Error')
%title(sprintf('ULA N=%d   Direction-Finding Error vs Delay using Uncorrelated Signals',N))
legend('SNR = -15 dB', 'SNR = -10 dB', 'SNR = -5 dB', 'SNR = 0 dB', 'SNR = +5 dB')

% Make sure the Y-axis starts at the origin
v = axis;
v = [ prng(1) prng(end) 0 y_max ];
axis(v)

% EOF
