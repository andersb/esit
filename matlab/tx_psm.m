%
% Generate time slot to be transmitted
%
% Anders Buvarp
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    sig        Carrier wave
%    ep         Slant angle
%    pdiff      Phase difference
%
% Output:
%
%    slot        Configuration
%

function slot = tx_psm(cfg,sig,ep,pdiff)

h = sig * ep; 
v = sig * exp(1j*pdiff);
slot = [h;v];

% EOF