%
% Plot heat map for polarization-space modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

% Load data
load ../data/dBm_25_x_260.mat

% Receiver position
rx_pos_x = 20;
rx_pos_y = -260;   % Fraunhofer region is 2D^2 / wavelength
x_rng = 1:rx_pos_x+5;
y_rng = 1:-rx_pos_y;

% Font and marker sizes
font_size = 40;
marker_size = 20;

figure('color','w');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

% Heat map
imagesc(y_rng-1,x_rng-1,dBm),hold
set(gca,'YDir','normal');
colormap default
cb = colorbar
ylabel(cb,'Power (dBm)','FontSize',font_size,'Rotation',270);
lbpos = get(cb,'ylabel');
pos = get(lbpos,'position'); 
pos(1) = pos(1) + 3;
set(lbpos, 'position', pos);
%title('Signal Power vs Location')
xlabel('-y [meters]')
ylabel('x [meters]')    
plot(-rx_pos_y,rx_pos_x,'x','Color','k','MarkerSize',marker_size)
plot(0,0.5,'d','Color','k','MarkerSize',marker_size)
plot(-rx_pos_y,rx_pos_x,'o','Color','k','MarkerSize',marker_size),hold
legend('Position of the receiver','Position of the RIS')
axis square

% EOF
