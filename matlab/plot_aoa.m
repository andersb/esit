%
% Plot DF error as a function of AoA for correlated and 
% uncorrelated signals
%
%
% Anders Buvarp
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    id             The ID number of the plot
%    rms_corr       DF error using correlated signals
%    rms_uncorr     DF error using uncorrelated signals
%    cfg            The configuration
%
function plot_aoa(id,rms_corr,rms_uncorr,cfg)

figure(id)

num_snr = 5;
nsnr = size(rms_corr,1);
nparams = size(rms_corr,2);
if nsnr < num_snr
    rms_corr((nsnr+1):num_snr,:) = zeros(num_snr-nsnr,nparams);
end % if
nsnr = size(rms_uncorr,1);
nparams = size(rms_uncorr,2);
if nsnr < num_snr
    rms_uncorr((nsnr+1):num_snr,:) = zeros(num_snr-nsnr,nparams);
end % if

% Range of parameters
prng = cfg.param_range;

% ULA size
N = cfg.N;

subplot(211)
plot(prng,rms_corr(1,:),'r.-'),grid,hold
plot(prng,rms_corr(2,:),'b--')
plot(prng,rms_corr(3,:),'m-.')
plot(prng,rms_corr(4,:),'k:')
plot(prng,rms_corr(5,:),'g-'),hold
xlabel('(a), Angle of Arrival [degrees]')
ylabel('Root Mean Square Error')
%title(sprintf('ULA N=%d   Direction-Finding Error vs Angle of Arrival using Correlated Signals',N))
legend('SNR = -15 dB', 'SNR = -10 dB', 'SNR = -5 dB', 'SNR = 0 dB', 'SNR = +5 dB')

% Make sure the Y-axis starts at the origin
y_max = ceil(max(max([rms_corr rms_uncorr])));
axis([ -90 90 0 y_max ]);

subplot(212)
plot(prng,rms_uncorr(1,:),'r.-'),grid,hold
plot(prng,rms_uncorr(2,:),'b--')
plot(prng,rms_uncorr(3,:),'m-.')
plot(prng,rms_uncorr(4,:),'k:')
plot(prng,rms_uncorr(5,:),'g-'),hold
xlabel({'(b), Angle of Arrival [degrees]'})
%xlabel({'Angle of Arrival [degrees]','(b)'})
ylabel('Root Mean Square Error')
%title(sprintf('ULA N=%d   Direction-Finding Error vs Angle of Arrival using Uncorrelated Signals',N))
legend('SNR = -15 dB', 'SNR = -10 dB', 'SNR = -5 dB', 'SNR = 0 dB', 'SNR = +5 dB')

% Make sure the Y-axis starts at the origin
axis([ -90 90 0 y_max ]);

% Re-size
set(gcf,'units','points','position',[100,100,500,300])

% EOF
