% https://scicomp.stackexchange.com/questions/6758/matlab-is-there-a-way-to-programatically-safely-halt-code-execution-like-fort
function stoprun()
  ms.message='';
  ms.stack = dbstack('-completenames');
  ms.stack(1:end) = [];
  ds = dbstatus();
  stoponerror = any(strcmp('error', {ds.cond}));
  setappdata(0, 'dberrorkeep', stoponerror);
  dbclear error
  error(ms);
end