%
% Generate random data and carrier wave
%
% Anders Buvarp
%
% References:
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg            Configuration
%
% Output:
%
%    carrier        The carrier wave
%    data           The source data to be transmitted
%

function [cfg,carrier,data] = psm_source(cfg)

% Random slant angles
msb = randi([0 1],1,cfg.num_code_words);

% Sign of phases
cfg.epsilon = 1 - 2*msb;

% Random phases
cfg.lsb3 = randi([0 7],1,cfg.num_code_words);
cfg.delta_phase = cfg.epsilon .* cfg.lsb3*pi/16;

% Generate transmission
num_samps = cfg.num_code_words * cfg.slot_len;
t = (0:num_samps)*cfg.Ts;
carrier = cfg.E0*exp(2j*pi*cfg.Fc*t);

% Save the source data to file
data = 8*msb + cfg.lsb3;
if cfg.save_src_flag 
    savename = sprintf('psm_labels_snr_%d.mat',cfg.snr_range);
    fname = fullfile('../data', savename);
    save(fname,'data');
end % if

% Debug
%h = cfg.E0*exp(2j*pi*cfg.Fc*t);
%p = -7*pi/16;
%v = -h .* exp(1j*p);
%slot = [h;v];

% EOF


