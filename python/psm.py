#!/usr/bin/env python3
"""
    Polarization-space modulation

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

"""

import datetime
import sys
import json
from sys import exit
import torch, numpy
import matplotlib.pyplot as plt
import numpy as np
from psm_dec import PSM_Net
import scipy.io as spio
import mat73
#import torch_xla
#import torch_xla.core.xla_model as xm

"""
##############################################################################

    Main entry point for the QNN

##############################################################################
"""

# Time
print(datetime.datetime.now())

# Load config
fd = open('psm.json')
cfg = json.load(fd)
fd.close()

# Flags
train_flag = cfg['train_flag']         # Train the network
eval_flag = cfg['test_flag']           # Test the network
sweep_flag = cfg['sweep_flag']         # Evaluate the network
plot_flag = cfg['plot_flag']           # Plotting
log_flag = cfg['log_flag']             # Store std out to a log file
tpu_flag = cfg['tpu_flag']             # Run on Colab TPU


# Device
if tpu_flag == True:
    """
    import os 
    os.environ['LD_LIBRARY_PATH']='/usr/local/lib'

    !echo $LD_LIBRARY_PATH
    !sudo ln -s /usr/local/lib/libmkl_intel_lp64.so /usr/local/lib/libmkl_intel_lp64.so.1
    !sudo ln -s /usr/local/lib/libmkl_intel_thread.so /usr/local/lib/libmkl_intel_thread.so.1
    !sudo ln -s /usr/local/lib/libmkl_core.so /usr/local/lib/libmkl_core.so.1

    !ldconfig
    !ldd /usr/local/lib/python3.7/dist-packages/torch/lib/libtorch.so

    !pip install cloud-tpu-client==0.10 https://storage.googleapis.com/tpu-pytorch/wheels/torch_xla-1.9-cp37-cp37m-linux_x86_64.whl
    device = xm.xla_device()
    """
    device = xm.xla_device()
else:
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

# Number of hidden layers
layers = cfg['layers']

# The input dimension
#n_feature = cfg['n_input']

# Number of hidden neurons
n_hidden = cfg['n_hidden']

# The output dimension
n_output = cfg['n_output']

# Hyperparameters
batch_size = cfg['batch_size']
epochs = cfg['epochs']
learning_rate = cfg['lr']
lr = learning_rate
decay = cfg['decay']
num_eval = cfg['num_eval']

# Data file
fname_train = cfg['quat_data_file']

# Load training data
mat_train = mat73.loadmat(fname_train)
quat_data = mat_train['quat']
lbls = mat_train['src']
snr_levels = mat_train['snr_range']

# Training data
quat_train = quat_data[:,:,num_eval:,:]
lbls_train = lbls[num_eval:]
snr_ix_train = cfg['snr_ix_train']

# Extract dimensions
iter_dim = 1
slots_dim = 2
feat_dim = 3               
n_iter = quat_train.shape[iter_dim]
n_slots = quat_train.shape[slots_dim]
n_feature = quat_train.shape[feat_dim]
batch_size = cfg['batch_size']

# Eval data
quat_eval = quat_data[:,:,:num_eval,:]
lbls_eval = lbls[:num_eval]
eval_iter = quat_eval.shape[iter_dim]
eval_slots = quat_eval.shape[slots_dim]
first,last = cfg['eval_snr_ix']
eval_snr_ix = np.arange(snr_levels[first],snr_levels[last]+1)

# Set std out
if log_flag == True:

    # Re-direct std out to a log file
    log_file = 'logs/log_iter_%d_slots_%d.txt' % (quat_train.shape[iter_dim],quat_train.shape[slots_dim])
    print('Log is written to %s' % log_file)
    sys.stdout = open(log_file,'w')

# Create the MLP network decoder
dec = PSM_Net(n_iter, n_slots, n_feature, n_hidden, n_output, batch_size, device)
dec = dec.double()
dec.to(device)
dec.print_set(lr,snr_levels[snr_ix_train],snr_levels[eval_snr_ix],num_eval)

# Model file name
fname_model = 'models/psm_layers_%d_iter_%d_slots_%d_batch_size_%d_epochs_%d.ptm' % (layers,n_iter,n_slots,batch_size,epochs)

# Check the Train Network flag
if train_flag == True:

    # Make the train loaders
    train_loader = dec.make_loader(quat_train, lbls_train, snr_ix_train, snr_levels, n_iter, n_slots, True)

    # Flush stdout to file
    sys.stdout.flush()

    # Train the network
    dec.train(train_loader, epochs, lr, decay)

    # Save MLP to file
    torch.save(dec.state_dict(), fname_model)

# Check the Eval Network flag
if eval_flag == True:

    # Load model
    dec.load_state_dict(torch.load(fname_model,map_location=torch.device(device)))

    # Test network
    eval_loader = dec.make_loader(quat_eval, lbls_eval, eval_snr_ix, snr_levels, eval_iter, eval_slots, False)
    ber,ser,acc = dec.accuracy(eval_loader)
    print('BER: %0.8e \t SER: %0.8e \t Average Accuracy: %0.2f percent\n' % (ber,ser,acc*100))

# Check for sweeping SNR
if sweep_flag == True:

    # Load model
    dec.load_state_dict(torch.load(fname_model,map_location=torch.device(device)))

    # Compute BER, SER and Accuracy for a range of SNR levels
    ber_list = []
    ser_list = []
    for snr_ix in eval_snr_ix:

        # Generate symbols at a particular SNR value
        sweep_loader = dec.make_loader(quat_eval, lbls_eval, [snr_ix], snr_levels, eval_iter, eval_slots, False)
        ber,ser,acc = dec.accuracy(sweep_loader)
        ber_list.append(ber)
        ser_list.append(ser)
        snr = snr_levels[snr_ix]
        print('SNR: %d \t BER: %.5f \t SER: %.5f \t Accuracy: %0.2f percent' % (snr,ber,ser,acc*100))

    print('res:',ber_list)

    # Plot accuracy
    
    leg = 'QNN layers: %d    epochs: %d   batch size: %d' % (layers,epochs,batch_size)
    snr = snr_levels[eval_snr_ix]
    plt.semilogy(snr, ber_list, color = "orange", label=leg)
    plt.xlim(snr[0]-1,snr[-1]+1)
    plt.ylim(10e-4,1)
    plt.grid(True)
    plt.title('RIS Polarization-State Modulation with Quaternion Neural Network    Bit-Error Rate vs SNR')
    plt.xlabel('SNR [dB]')
    plt.ylabel('BER')
    plt.legend()
    fname_plot = '../plots/psm_layers_%d_iter_%d_slots_%d_batch_size_%d_epochs_%d.png' % (layers,n_iter,n_slots,batch_size,epochs)
    plt.savefig(fname_plot)
    if plot_flag == True:
        plt.show()

    # Save accuracy to file
    fname_acc = '../accuracy/psm_layers_%d_iter_%d_slots_%d_batch_size_%d_epochs_%d.txt' % (layers,n_iter,n_slots,batch_size,epochs)
    np.savetxt(fname_acc,np.asarray(ber_list), delimiter=',')

# Flush to stdout
sys.stdout.flush()
    
# Time
print(datetime.datetime.now())

# EOF
