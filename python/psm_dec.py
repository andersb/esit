"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul and Vijay Mishra 
"""

import sys
from sys import exit
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.optim as optim
sys.path.insert(0, '../../Quaternion-Neural-Networks')
from core_qnn.quaternion_layers import *

#
# Three activation functions
#

# HReLU
def HReLU(x):
    x[0,0,:] = torch.relu(x[0,0,:])
    x[0,1,:] = torch.relu(x[0,1,:])
    x[0,2,:] = torch.relu(x[0,2,:])
    x[0,3,:] = torch.relu(x[0,3,:])
    #print(x)
    return x

# ModReLU
# relu(|z|+b) * (z / |z|)
# b is bias
def ModReLU(input_r,input_i,b):
    norm = torch.sqrt(input_r*input_r+input_i*input_i)
    scale = torch.relu(norm + b) / (norm + 1e-6)
    return input_r*scale,input_i*scale

# zReLU
def zReLU(input_r,input_i):
    rmask = torch.ge(input_r, torch.zeros_like(input_r)).double()
    imask = torch.ge(input_i, torch.zeros_like(input_i)).double()
    mask = rmask * imask
    return input_r*mask,input_i*mask

class PSM_Net(nn.Module):
    def __init__(self, n_iter, n_slots, n_input, n_hidden, n_output, batch_size, device):
        super(PSM_Net, self).__init__()

        self.n_iter = n_iter
        self.n_slots = n_slots
        self.n_data = n_iter*n_slots
        self.inp_dim = n_input
        self.hid_dim = n_hidden
        self.out_dim = n_output
        self.batch_size = batch_size
        
        self.fc1    = QuaternionLinear(n_input, n_hidden)
        self.fc2    = QuaternionLinear(n_hidden, 2*n_hidden)
        self.fc3    = QuaternionLinear(2*n_hidden, 4*n_hidden)
        self.fc4    = QuaternionLinear(4*n_hidden, 2*n_hidden)
        self.fc5    = QuaternionLinear(2*n_hidden, n_hidden)
        self.fcOut  = nn.Linear(n_hidden, n_output)
        
        # The processing device; CPU or GPU
        self.device = device
        print('-----------------------')
        print('Created PSM QNN decoder')
        print('-----------------------')
		
    def forward(self, x):
        
        x = x.view(x.shape[0], -1)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x)
        x = F.relu(x)
        x = self.fc3(x)
        x = F.relu(x)
        x = self.fc4(x)
        x = F.relu(x)
        x = self.fc5(x)
        x = F.relu(x)
        x = self.fcOut(x)
        return x

    def train(self, trainloader, epochs, lr, decay):
        
        super().train()
        
        # Use cross-entropy for loss function
        criterion = nn.CrossEntropyLoss()
        
        # Use the Adam optimizer
        optimizer = optim.Adam(self.parameters(), lr=lr, weight_decay=decay)

        # Number of examples in trainloader
        num = len(trainloader)*self.batch_size
        print('Training with %d examples for %d epochs' % (num,epochs))
        
        # Loss counter
        prev_loss = 1
        loss_counter = 0
        loss_count = 20
        loss_threshold = 0.95
        lr_reduction_fraction = 0.85
 
        # For each epoch
        for epoch in range(epochs):
            running_loss = 0.0
            sys.stdout.flush()
            for count, data in enumerate(trainloader,0):
                
                # Reset optimizer
                optimizer.zero_grad()
                
                # Extract data from trainloader
                inp, lbl = data
                
                # Forward propagation
                outp = self(inp)

                # Backward propagation
                loss = criterion(outp, lbl.long())
                loss.backward()
                optimizer.step()
                running_loss += loss.item()

            # Compute average loss
            ave_loss = running_loss / num

            # Compute relative/diff loss
            rel_loss = ave_loss / prev_loss
            di = ave_loss - prev_loss
            prev_loss = ave_loss

            # Update loss counter
            loss_counter += 1

            # Check if we should lower he learning rate
            if loss_counter == loss_count and rel_loss > loss_threshold:
                lr *= lr_reduction_fraction
                optimizer = optim.Adam(self.parameters(), lr=lr, weight_decay=decay)        
                loss_counter = 0

            print('Epoch: %d\tLoss: %.3e\t\tDiff: %.3e\tCount: %d\tLearning Rate: %.5f' % (epoch,ave_loss,di,num,lr))
            #print('Loss Counter: %d\tRel loss change: %e' % (loss_counter,ar))

        print('Finished Training')

    def accuracy(self, testloader):

        #super().eval()
        test_loss = 0
        #correct = 0

        # Bit/symbol error
        sum_berr = 0
        symb_err = 0
        
        loss_f = nn.CrossEntropyLoss(reduction='sum')
        
        with torch.no_grad():
            for data, lbls in testloader:
                data, lbls = data.to(self.device), lbls.to(self.device)

                # Forward propagation
                outp = self(data)
                
                # Sum up batch loss
                labels = lbls.data.long()
                
                # Bit errors
                _, predicted = torch.max(outp.data, 1)
                bit_diff = torch.bitwise_xor(labels,predicted)
                ar = bit_diff.cpu().numpy()
                for x in ar:
                    count = bin(x).count("1")
                    sum_berr += count
                
                # Symbol errors
                symb_err += (predicted != labels).sum().item()

                # Accuracy
                test_loss += loss_f(outp, labels).item()

                # Get the index of the max log-probability
                #pred = outp.argmax(dim=1, keepdim=True)  
                #correct += pred.eq(lbls.view_as(pred)).sum().item()
                
                
        total = len(testloader)*self.batch_size
        ber = sum_berr / total
        ser = symb_err / total
        acc = 1 - test_loss / total
        return ber,ser,acc
    
    # Generate a dataloader
    def make_loader(self, data_feat, lbls, snr_ix, snr_levels, n_iter, n_slots, shuffle):
        
        # Index to data
        dx = np.arange(0,n_slots)
 
        # Number of SNR values
        num_snr = np.size(snr_ix)
        
        # Extract dimensions
        D = self.inp_dim
        num_data = n_slots * n_iter
        L = num_data * num_snr

        # Allocate memory for features and labels
        feat = np.empty((L,D))
        labs = np.empty(L)

        # Loop for the snr range
        for six in snr_ix:
            
            # Loop for the iterations
            ix = 0
            for it in range(0,n_iter):

                # Save the labels
                labs[dx] = lbls

                # Copy all slots
                feat[dx,:] = data_feat[six,ix,:,:]

                # Update the data index
                dx += n_slots
                ix += 1
                                       
            print("Generated %d examples with SNR %d dB" % (num_data,snr_levels[six]))

        # Append labels to the data
        data_list = []
        for lx in range(L):
            f = torch.DoubleTensor(np.array([feat[lx,:]])).to(torch.device(self.device))
            l = torch.DoubleTensor(np.array(labs[lx])).to(torch.device(self.device))
            data_list.append([f,l])

        # Make DataLoader
        loader = torch.utils.data.DataLoader(data_list, batch_size=self.batch_size, shuffle=shuffle)
        return loader
    
    # Print parameters
    def print_set(self,lr,train,evalu,num_eval):

        print('Device:\t\t\t\t',                    self.device)
        print('Learning Rate:\t\t\t',               lr)
        print('SNR Training Levels [dB]:\t',        train)
        print('SNR Eval Range [dB]:\t\t [%d,%d]' % (evalu[0],evalu[-1]))
        print('Feature Dimension:\t\t',             self.inp_dim)
        print('Number of Iterations:\t\t',          self.n_iter)
        print('Number of Slots:\t\t',               self.n_slots)
        print('Batch Size:\t\t\t',                  self.batch_size)
        print('Num Eval Examples:\t\t',             num_eval)

    def plot(self,x,y,z,ylabel,zlabel,snr_min,snr_max):
        
        plt.figure(figsize=(10,6))
        plt.scatter(x, y, color = "orange", label=ylabel)
        plt.scatter(x, z, color = "red", label=zlabel)
        plt.title('Alpha vs Alpha Hat    SNR Range: %d dB to %d dB' % (snr_min,snr_max))
        plt.xlabel('Alpha')
        plt.ylabel('Alpha Hat')
        plt.grid(True)
        plt.legend()
        plt.savefig("regr.png")

# EOF
